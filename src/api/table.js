import request from '@/utils/request'
const myBaseURL = ''
export function getList(params) {
  return request({
    baseURL: myBaseURL,
    url: '/getAllByTag',
    method: 'get',
    params
  })
}

export function updateTagBySend(params) {
  return request({
    baseURL: myBaseURL,
    url: '/updateTagBySend',
    method: 'post',
    params
  })
}

export function updateTagByLocatorCode(params) {
  return request({
    baseURL: myBaseURL,
    url: '/updateTagByLocatorCode',
    method: 'post',
    params
  })
}

export function excelImportByTags(params) {
  return request({
    baseURL: myBaseURL,
    url: '/excelImportByTags',
    method: 'post',
    data: params
  })
}
