import request from '@/utils/request'
const myBaseURL = ''
export function login(username, password) {
  return request({
    baseURL: myBaseURL,
    url: '/user/login',
    method: 'post',
    data: {
      username,
      password
    }
  })
}

export function getInfo(token) {
  return request({
    baseURL: myBaseURL,
    url: '/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    baseURL: myBaseURL,
    url: '/user/logout',
    method: 'post'
  })
}
